import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
  MatButtonModule,
  MatCardModule,
  MatMenuModule,
  MatToolbarModule,
  MatListModule,
  MatTableModule,
  MatFormFieldModule,
  MatDatepickerModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule,
  MatStepperModule,
  MatIconModule
} from '@angular/material';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';

import { Routes, RouterModule } from '@angular/router';


import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { DoctorListComponent } from './components/admin/doctor/doctor-list/doctor-list.component';
import { DoctorCreateComponent } from './components/admin/doctor/doctor-create/doctor-create.component';
import { DoctorEditComponent } from './components/admin/doctor/doctor-edit/doctor-edit.component';
import { DoctorDetailsComponent } from './components/admin/doctor/doctor-details/doctor-details.component';
import { ClinicCreateComponent } from './components/admin/clinic/clinic-create/clinic-create.component';
import { ClinicDetailsComponent } from './components/admin/clinic/clinic-details/clinic-details.component';
import { ClinicEditComponent } from './components/admin/clinic/clinic-edit/clinic-edit.component';
import { ClinicListComponent } from './components/admin/clinic/clinic-list/clinic-list.component';
import { PatientCreateComponent } from './components/admin/patient/patient-create/patient-create.component';
import { PatientDetailsComponent } from './components/admin/patient/patient-details/patient-details.component';
import { PatientEditComponent } from './components/admin/patient/patient-edit/patient-edit.component';
import { PatientListComponent } from './components/admin/patient/patient-list/patient-list.component';
import { ScheduleCreateComponent } from './components/admin/schedule/schedule-create/schedule-create.component';
import { ScheduleDetailsComponent } from './components/admin/schedule/schedule-details/schedule-details.component';
import { ScheduleEditComponent } from './components/admin/schedule/schedule-edit/schedule-edit.component';
import { ScheduleListComponent } from './components/admin/schedule/schedule-list/schedule-list.component';
import { VisitCreateComponent } from './components/admin/visit/visit-create/visit-create.component';
import { VisitDetailsComponent } from './components/admin/visit/visit-details/visit-details.component';
import { VisitEditComponent } from './components/admin/visit/visit-edit/visit-edit.component';
import { VisitListComponent } from './components/admin/visit/visit-list/visit-list.component';
import { UserDetailsComponent } from './components/admin/user/user-details/user-details.component';
import { UserEditComponent } from './components/admin/user/user-edit/user-edit.component';
import { UserListComponent } from './components/admin/user/user-list/user-list.component';
import { AbstractCreateComponent } from './components/abstract-create/abstract-create.component';
import { RoleEditComponent } from './components/admin/user/role-edit/role-edit.component';
import { LoginComponent } from './components/login/login.component';
import { AddHeaderInterceptor } from './services/add-header.intercepor';
import { AuthGuardAdminService } from './services/auth-guard-admin.service';
import { PatientDashboardComponent } from './components/patient/patient-dashboard/patient-dashboard.component';
import { LogoutComponent } from './components/logout/logout.component';
import { PatientProfileComponent } from './components/patient/patient-profile/patient-profile.component';
import { PatientVisitListComponent } from './components/patient/patient-visit-list/patient-visit-list.component';
import { PatientCreateVisitComponent } from './components/patient/patient-create-visit/patient-create-visit.component';
import { AuthGuardPatientService, AuthGuardPatientWithUserService } from './services/auth-guard-patient.service';
import { RegisterComponent } from './components/register/register.component';
import { PatientScheduleComponent } from './components/patient/patient-schedule/patient-schedule.component';
import { PatientScheduleListComponent } from './components/patient/patient-schedule-list/patient-schedule-list.component';
import { AppMaterialModules } from './material.module';
import { PatientScheduleVisitCreationComponent } from './components/patient/patient-schedule-visit-creation/patient-schedule-visit-creation.component';
import { PatientVisitCancelComponent } from './components/patient/patient-visit-cancel/patient-visit-cancel.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'logout',                    component: LogoutComponent},
  {path: 'login',                     component: LoginComponent},
  {path: 'register',                  component: RegisterComponent},
  // admin
  { path: 'admin', component: DashboardComponent, canActivate: [AuthGuardAdminService],
    children: [
      { path: '', redirectTo: 'doctor-list', pathMatch: 'full'},
      { path: 'doctor-list',          component: DoctorListComponent},
      { path: 'doctor-create',        component: DoctorCreateComponent},
      { path: 'doctor-edit/:id',      component: DoctorEditComponent},
      { path: 'doctor-details/:id',   component: DoctorDetailsComponent},
      { path: 'clinic-list',          component: ClinicListComponent },
      { path: 'clinic-create',        component:  ClinicCreateComponent },
      { path: 'clinic-edit/:id',      component:  ClinicEditComponent },
      { path: 'clinic-details/:id',   component:  ClinicDetailsComponent },
      // patient
      { path: 'patient-list',         component: PatientListComponent },
      { path: 'patient-create',       component: PatientCreateComponent },
      { path: 'patient-edit/:id',     component: PatientEditComponent },
      { path: 'patient-details/:id',  component: PatientDetailsComponent },
      // schedule
      { path: 'schedule-list',        component: ScheduleListComponent },
      { path: 'schedule-create',      component: ScheduleCreateComponent },
      { path: 'schedule-edit/:id',    component: ScheduleEditComponent },
      { path: 'schedule-details/:id', component: ScheduleDetailsComponent },
      // visit
      { path: 'visit-list',           component: VisitListComponent },
      { path: 'visit-create',         component: VisitCreateComponent },
      { path: 'visit-edit/:id',       component: VisitEditComponent },
      { path: 'visit-details/:id',    component: VisitDetailsComponent },
      // user
      { path: 'user-list',            component: UserListComponent },
      { path: 'user-edit/:id',        component: UserEditComponent },
      { path: 'user-details/:id',     component: UserDetailsComponent }
    ]
  },
  { path: 'patient', component: PatientDashboardComponent,
    children: [
      { path: '', redirectTo: 'profile', pathMatch: 'full'},
      { path: 'profile',      component: PatientProfileComponent, canActivate: [AuthGuardPatientService]},
      { path: 'visit-list',   component: PatientVisitListComponent, canActivate: [AuthGuardPatientWithUserService]},
      { path: 'visit-create', component: PatientCreateVisitComponent, canActivate: [AuthGuardPatientWithUserService]}
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DoctorListComponent,
    DoctorCreateComponent,
    DoctorEditComponent,
    DoctorDetailsComponent,
    ClinicCreateComponent,
    ClinicDetailsComponent,
    ClinicEditComponent,
    ClinicListComponent,
    PatientCreateComponent,
    PatientDetailsComponent,
    PatientEditComponent,
    PatientListComponent,
    ScheduleCreateComponent,
    ScheduleDetailsComponent,
    ScheduleEditComponent,
    ScheduleListComponent,
    VisitCreateComponent,
    VisitDetailsComponent,
    VisitEditComponent,
    VisitListComponent,
    UserDetailsComponent,
    UserEditComponent,
    UserListComponent,
    AbstractCreateComponent,
    RoleEditComponent,
    LoginComponent,
    PatientDashboardComponent,
    LogoutComponent,
    PatientProfileComponent,
    PatientVisitListComponent,
    PatientCreateVisitComponent,
    RegisterComponent,
    PatientScheduleComponent,
    PatientScheduleListComponent,
    PatientScheduleVisitCreationComponent,
    PatientVisitCancelComponent
  ],
  exports: [
    PatientVisitCancelComponent,
    VisitEditComponent
  ],
  entryComponents: [
    PatientVisitCancelComponent,
    VisitListComponent,
    VisitEditComponent
  ],
  imports: [
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    // Material Modules
    AppMaterialModules
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AddHeaderInterceptor,
    multi: true,
  },
  AuthGuardAdminService,
  AuthGuardPatientService
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
