import { TestBed, inject } from '@angular/core/testing';

import { AuthGuardPatientService } from './auth-guard-patient.service';

describe('AuthGuardPatientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuardPatientService]
    });
  });

  it('should be created', inject([AuthGuardPatientService], (service: AuthGuardPatientService) => {
    expect(service).toBeTruthy();
  }));
});
