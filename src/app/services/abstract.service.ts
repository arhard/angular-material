import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';

/**
 * Class implementing abstract service with basic funcionalities
 * @param T DTO type
 */
export abstract class AbstractService<T> {

  /**
   * Constructor
   * @param url Base url
   * @param http http client used for request
   */
  constructor(protected url: string, protected http: HttpClient) { }

  /**
   * Sends post request to base url
   * @param body dto body that will be send as a post request
   * @returns promise with response from the server
   */
  async create(body: T): Promise<T> {
    // console.log(`AbstractService: create, url: ${this.url}`);
    return <T>await this.http.post(this.url, body).toPromise();
  }

  /**
   * Sends get request to base url
   * @returns promise with response from the server
   */
  async get(): Promise<T[]> {
    // console.log(`AbstractService: get, url: ${this.url}`);
    return <T[]>await this.http.get(this.url).toPromise();
  }

  /**
   * Sends get request to base url and concatenates resource id
   * @param id resource id
   * @returns promise with response from the server
   */
  async getById(id: string): Promise<T> {
    // console.log(`AbstractService: getById, url: ${this.url}/${id}`);
    return <T>await this.http.get(`${this.url}/${id}`).toPromise();
  }

  /**
   * Sends pach request to base url and pases dto body
   * @param body body of updated element
   * @returns promise with response from the server
   */
  async update(body: T): Promise<T> {
    // console.log(`AbstractService: update, url: ${this.url}/${(<any>body)._id}`);
    return  (<Observable<T>>this.http.patch(`${this.url}/${(<any>body)._id}`, body)).toPromise<T>();
  }

  /**
   * Sends delete request to base url and concatenates resource id
   * @param id resource id
   * @returns promise with response from the server
   */
  async delete(id: string): Promise<T> {
    // console.log(`AbstractService: delete, url: ${this.url}/${id}`);
    return <T>await this.http.delete(`${this.url}/${id}`).toPromise();
  }
}
