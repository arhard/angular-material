import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { HttpClient } from '@angular/common/http';

export interface IUser {
  _id?: string;
  email?: string;
  password?: string;
  roles?: string[];
}

@Injectable({
  providedIn: 'root'
})
export class UserService extends AbstractService<IUser> {

  constructor(protected http: HttpClient) {
    super('http://localhost:3000/api/users', http);
  }
}
