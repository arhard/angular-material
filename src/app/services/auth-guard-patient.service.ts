import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { PatientService } from './patient.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardPatientService {

  constructor(
    private authService: AuthService,
    private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.isPatient()) {
      return true;
    }
    this.router.navigateByUrl('/login');
    return false;
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthGuardPatientWithUserService {

  constructor(
    private patientService: PatientService,
    private authService: AuthService,
    private router: Router) {}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.isPatient()) {
      const patient = await this.patientService.getByCurrentUsersCredentials();
      if (!patient) {
        this.router.navigateByUrl('/login');
        return false;
      }
      return true;
    }
    this.router.navigateByUrl('/login');
    return false;
  }
}
