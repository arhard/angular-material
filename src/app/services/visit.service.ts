import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractService } from './abstract.service';
import { ISchedule } from './schedule.service';
import { IPatient } from './patient.service';

export interface IVisit {
  _id?: string;
  start?: Date;
  end?: Date;
  reason?: string;
  completed?: boolean;
  canceled?: boolean;
  cancelation_reason?: string;
  schedule_id?: any | ISchedule;
  patient_id?: any | IPatient;
}


@Injectable({
  providedIn: 'root'
})
export class VisitService extends AbstractService<IVisit> {
  constructor(protected http: HttpClient) {
    super('http://localhost:3000/api/visits', http);

  }

  async getByScheduleId(id: string): Promise<IVisit[]> {
    return <Array<IVisit>>await this.http.get(`${this.url}/byschedule/${id}`).toPromise();
  }


  async cancelVisit(id: string, reason: string) {
    // these fields needs to be set
    await this.update({
      _id: id,
      canceled: true,
      cancelation_reason: reason
    });
  }
}
