import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { HttpClient } from '@angular/common/http';


export interface IDoctor {
  _id?: string;
  first_name?: string;
  last_name?: string;
  specialities?: string[] | any;
  schedules?: any[];
}

@Injectable({
  providedIn: 'root'
})
export class DoctorService extends AbstractService<IDoctor> {

  constructor(protected http: HttpClient) {
    super('http://localhost:3000/api/doctors', http);
  }

  async insertSchedule(id: string, schedule_id: string) {
    console.log('insertSchedule');
    return await this.http.post(`${this.url}/${id}/schedules`, {_id: schedule_id}).toPromise();
  }

  async removeSchedule(id: string, schedule_id: string) {
    console.log('removeSchedule');
    return await this.http.delete(`${this.url}/${id}/schedules/${schedule_id}`).toPromise();
  }
}
