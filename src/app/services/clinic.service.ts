import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { HttpClient } from '@angular/common/http';

export interface IClinic {
  _id?: any;
  name?: string;
  country?: string;
  postal_code?: string;
  city?: string;
  street?: string;
  building_number?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ClinicService extends AbstractService<IClinic> {

  constructor(protected http: HttpClient) {
    super('http://localhost:3000/api/clinics', http);
  }
}
