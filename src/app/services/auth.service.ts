import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '../../../node_modules/@angular/router';


export interface UserCredentials {
  id: string;
  email: string;
  token: string;
  roles: string[];
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userCredentials: UserCredentials;

  constructor(private http: HttpClient, private router: Router) {
  }
  async register(email: string, password) {
    try {
      const result = await this.http.post('http://localhost:3000/api/users/signup', {email: email, password: password}).toPromise();
      console.log('registration resul: ', result);
      this.router.navigateByUrl('/login');
    } catch (error) {
      throw error;
    }
  }
  async login(email: string, password: string) {
    this.userCredentials
    = <UserCredentials>await this.http.post('http://localhost:3000/api/users/signin', {email: email, password: password}).toPromise();

    // console.log('credentials: ', this.userCredentials);
    localStorage.setItem('user_credentials_token', this.userCredentials.token);
    localStorage.setItem('user_credentials_role', this.userCredentials.roles[0]);
    localStorage.setItem('user_credentials_email', this.userCredentials.email);
    localStorage.setItem('user_credentials_id', this.userCredentials.id);
    return this.userCredentials;
  }
  logout() {
    this.userCredentials = undefined;
    localStorage.removeItem('user_credentials_token');
    localStorage.removeItem('user_credentials_role');
    localStorage.removeItem('user_credentials_email');
    localStorage.removeItem('user_credentials_id');


  }
  redirectOnLogin() {
    console.log('redirect');
    const role = localStorage.getItem('user_credentials_role');
    if (role) {
      this.router.navigateByUrl(`/${role}`);
    }

  }
  getToken(): string {
    return localStorage.getItem('user_credentials_token');
  }

  hasToken(): boolean {
    // console.log('token:', localStorage.getItem('user_credentials_token'));
    if (localStorage.getItem('user_credentials_token')) {
      return true;
    }
    return false;
  }

  isAdmin(): boolean {
    return localStorage.getItem('user_credentials_role') === 'admin';
  }
  isDoctor(): boolean {
    return localStorage.getItem('user_credentials_role') === 'doctor';
  }
  isPatient(): boolean {
    return localStorage.getItem('user_credentials_role') === 'patient';
  }
  getCredentials() {
    // console.log('getCredentials()', this.userCredentials);
    if (this.userCredentials !== undefined) {
      // console.log('Credentials from object');
      return this.userCredentials;
    } else {
      this.userCredentials = {
        email: localStorage.getItem('user_credentials_email'),
        id: localStorage.getItem('user_credentials_id'),
        token: localStorage.getItem('user_credentials_token'),
        roles: [localStorage.getItem('user_credentials_role')],
      };
      // console.log('Credentials from localStorage:', this.userCredentials);
      return this.userCredentials;
    }
  }
}
