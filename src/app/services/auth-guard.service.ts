import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { RouterStateSnapshot, Router, ActivatedRoute, ActivatedRouteSnapshot } from '../../../node_modules/@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private auth: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  }
}
