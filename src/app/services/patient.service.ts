import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { HttpClient } from '@angular/common/http';
import { IVisit, VisitService } from './visit.service';
import { AuthService } from './auth.service';
import { IUser } from './user.service';

export interface IPatient {
  _id?: any;
  first_name?: string;
  last_name?: string;
  national_number?: string;
  user_id?: IUser | any;
  visits?: IVisit[];
}

const url = 'http://localhost:3000/api/patients';

@Injectable({
  providedIn: 'root'
})
export class PatientService extends AbstractService<IPatient> {

  constructor(protected http: HttpClient, protected auth: AuthService, protected visitService: VisitService) {
    super(url, http);
  }

  // inserts visit's id into visits array in patient
  async insertVisit(visit: IVisit) {
    console.log(`PatientService: insertVisit ${url}/${visit.patient_id}/visits/`);
    const result = await this.http.post(`${url}/${visit.patient_id}/visits/`, visit).toPromise();
    console.log(`PatientService: insertVisit result:`, result);
    return result;
  }
  async getByUserId(id: string): Promise<IPatient> {
    console.log(`PatientService: getByUserId, url: ${url}/user/${id}`);
    const patient = await this.http.get(`${url}/user/${id}`).toPromise();
    console.log('Patient: ', patient);
    return patient;
  }


  async getByCurrentUsersCredentials(): Promise<IPatient> {
    return await this.http.get(`${url}/user/${this.auth.getCredentials().id}`).toPromise();
  }

  // check if current user has a patient profile
  async hasUser() {
    const id = this.auth.getCredentials().id;
    console.log(`PatientService: hasUser, url: ${url}/user/${id}`);
    const patient = await this.http.get(`${url}/user/${id}`).toPromise();
    return (patient !== null);
  }

  /**
   * After patient is deleted there is no reason in keeping his visits
   */
  async deleteVisits(id) {
    const patient = await this.getById(id);
    console.log('deleteVisits patient', patient);
    patient.visits.forEach(async visit => {
      await this.visitService.delete(visit._id);
    });
  }
}
