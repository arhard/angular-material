import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '../../../node_modules/@angular/core';

@Injectable()
export class AddHeaderInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add JWT token to every request
    if (this.auth.hasToken()) {

      const clonedRequest = req.clone({ headers: req.headers.set('Authorization', this.auth.getToken())});
      // pass cloned request with added token
      return next.handle(clonedRequest);
    }
    return next.handle(req);
  }
}
