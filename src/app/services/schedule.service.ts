import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AbstractService } from './abstract.service';

import { IClinic } from './clinic.service';
import { IDoctor } from './doctor.service';
import { IVisit, VisitService } from './visit.service';
import { Moment } from '../../../node_modules/moment';
import * as moment from 'moment';

export interface ISchedule {
  _id?: any;
  start?: Date;
  end?: Date;
  doctor_id?: IDoctor | any;
  clinic_id?: IClinic;
  visits?: IVisit[];
}

@Injectable({
  providedIn: 'root'
})
export class ScheduleService extends AbstractService<ISchedule> {

  constructor(protected http: HttpClient, private visitService: VisitService) {
    super('http://localhost:3000/api/schedules', http);
  }

  async insertVisit(visit: IVisit) {
    console.log(`ScheduleService: insertVisit, url: ${this.url}/${visit.schedule_id}/visits/`);
    const result = await this.http.post(`${this.url}/${visit.schedule_id}/visits/`, visit).toPromise();
    return result;
  }

  async findBetweenDates(start: Date, end: Date): Promise<ISchedule[]> {
    console.log(`ScheduleService: findBetweenDates(), url: ${this.url}/bydate/?start=${moment(start).format('YYYY-MM-DD')}&end=${moment(end).format('YYYY-MM-DD')}`);
    const schedules = <Array<ISchedule>>await this.http
    .get(`${this.url}/bydate/?start=${moment(start).format('YYYY-MM-DD')}&end=${moment(end).format('YYYY-MM-DD')}`).toPromise();
    // .../schedules/bydate/?start=2018-07-10&end=2018-08-01

    console.log('Znalazłem następujące grafiki: ', schedules);
    schedules.forEach(async schedule => schedule.visits = await this.visitService.getByScheduleId(schedule._id));

    return schedules;
  }
  // resets hours and minutes in a date
  // it's used during submiting a date because seting a time in a date
  // works by adding an hour and minute offset to 00:00
  resetHoursMinutes(date: Moment) {
    date.set('hours', 0);
    date.set('minutes', 0);
    date.set('seconds', 0);
    return date;
  }

  /**
   * Changes date withouth changing hours
   * @param date new date
   * @param schedule schedule to be modified
   * @returns updated schedule
   */
  changeDateWithoutTime(date: Date, schedule: ISchedule) {
    schedule.start = new Date(schedule.start);
    schedule.end = new Date(schedule.end);

    schedule.start.setFullYear(date.getFullYear(), date.getMonth(), date.getDate());
    schedule.end.setFullYear(date.getFullYear(), date.getMonth(), date.getDate());
    return schedule;
  }

  // combines data from date and time input
  getDateTime(date: Date, time: string) {
    // time is in HH:MM format to it has to be splited for individual time and minutes part
    const timeRegexp = /([0-1][0-9]|2[0-3]):[0-5][0-9]/;
    if (!timeRegexp.test(time)) {
      throw new Error('Podana godzina jest w niepoprawnym formacie, spodziewałem się HH:MM');
    }

    const momentDate = moment(date);
    const time_hours_minutes = time.split(':');
    const hours = parseInt(time_hours_minutes[0], 10);
    const minutes = parseInt(time_hours_minutes[1], 10);

    // reset hours and minutes so provided ones can be added
    const newDate = this.resetHoursMinutes(momentDate);
    newDate.add(hours, 'hours');
    newDate.add(minutes, 'minutes');

    // return new date
    return newDate.toDate();
  }

  // calculate free spots in doctor's schedule so the patient can choose them
  calculateAvailableHours(schedule: ISchedule) {
     // generate timestamps in 30 minute time intervals
     let times: string[] = [];

     const start = moment(schedule.start);
     while (start.isBefore(schedule.end)) {
        times.push(start.format('HH:mm'));
        start.add(30, 'minutes');
     }
     // for every visit at this schedule filter out time from times that is equal to some visit's time
     // unless the visit is canceled
     schedule.visits.forEach(visit => times = times.filter(time => time !== moment(visit.start).format('HH:mm') || visit.canceled));
     return times;
  }
}
