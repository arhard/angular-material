import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(private http: HttpClient, private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.auth.redirectOnLogin();
  }

  async onSubmit() {
    if (!this.email || !this.password) {
      return;
    }
    await this.auth.login(this.email, this.password);
    this.auth.redirectOnLogin();
  }

  register() {

  }

}
