import { AbstractService } from '../../services/abstract.service';
import { Router } from '@angular/router';
import { OnInit, ViewChild, AfterViewInit, OnChanges } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatTab, MatSort } from '@angular/material';

export class AbstractListComponent<T> implements OnInit {
  protected dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(protected dataService: AbstractService<T>,
    protected router: Router,
    protected prefix: string,
    protected displayedColumns: string[]) {
  }

  async ngOnInit() {
    this.dataSource = new MatTableDataSource(<Array<any>>await this.dataService.get());
    this.dataSource.sort = this.sort;
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;

    }, 100);
  }

  async delete(id) {
    try {
      await this.dataService.delete(id);
      this.dataSource.data = this.dataSource.data.filter(element => element._id !== id);
      this.dataSource.paginator = this.paginator;
    } catch (error) {
      console.log(error);
    }

  }

  async edit(id) {
    this.router.navigateByUrl(`/admin/${this.prefix}-edit/${id}`);
  }

  async details(id) {
    this.router.navigateByUrl(`/admin/${this.prefix}-details/${id}`);
  }

  async add() {
    this.router.navigateByUrl(`/admin/${this.prefix}-create`);
  }
}
