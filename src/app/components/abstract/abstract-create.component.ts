import { OnInit } from '@angular/core';
import { AbstractService } from '../../services/abstract.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

export abstract class AbstractCreateComponents<T> implements OnInit {
  protected element: T;
  constructor(public snackBar: MatSnackBar, protected service: AbstractService<T>, protected router: Router, protected prefix: string, protected requiredFields?: string[]) {
    this.element = <T>{};
  }

  ngOnInit(): void {

  }

  async onSubmit() {
    try {
      if (this.requiredFields !== undefined) {
        this.requiredFields.forEach(field => {
          if (this.element[field] === undefined) {
            throw new Error('Brakuje pola: ' + field);
          }
        });
      }

      console.log(this.element);
      await this.service.create(this.element);
      this.router.navigateByUrl(`/admin/${this.prefix}-list`);
    } catch (error) {
      console.log(error);
      if (error.error.errors) {
        let message = '';
        for (const property in error.error.errors) {
          if (error.error.errors.hasOwnProperty(property)) {
            message += error.error.errors[property].message + ' ';
          }
        }
        this.snackBar.open(message, 'Zamknij', {duration: 5000});
      } else {
        this.snackBar.open(error.message, 'Zamknij' , {duration: 5000});
      }
    }
  }
}
