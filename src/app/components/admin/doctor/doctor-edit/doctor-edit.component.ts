import { Component, OnInit } from '@angular/core';
import { DoctorService, IDoctor } from '../../../../services/doctor.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-doctor-edit',
  templateUrl: './doctor-edit.component.html',
  styleUrls: ['./doctor-edit.component.css']
})
export class DoctorEditComponent implements OnInit {

  doctor: IDoctor;

  constructor(private doctorService: DoctorService, private router: Router, private route: ActivatedRoute) {
    this.doctor = {};
    this.doctor.specialities = [];
   }

  async ngOnInit() {
    await this.route.params.subscribe(async params => {
      this.doctor = await this.doctorService.getById(params.id);
    });
  }

  async onSubmit() {
    try {
      if (this.doctor.specialities.split === 'function') {
        this.doctor.specialities = this.doctor.specialities.split(',');
      }
      await this.doctorService.update(this.doctor);
      this.router.navigateByUrl('/admin/doctor-list');
    } catch (error) {
      alert(error.error.error);
    }
  }

}
