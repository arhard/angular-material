import { Component, OnInit } from '@angular/core';
import { IDoctor, DoctorService } from '../../../../services/doctor.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '../../../../../../node_modules/@angular/material';

@Component({
  selector: 'app-doctor-create',
  templateUrl: './doctor-create.component.html',
  styleUrls: ['./doctor-create.component.css']
})
export class DoctorCreateComponent implements OnInit {
  doctor: IDoctor;

  constructor(public snackBar: MatSnackBar, private doctorService: DoctorService, private router: Router) {
    this.doctor = {};
    this.doctor.specialities = [];
   }

  ngOnInit() {
  }

  async onSubmit() {
    try {
      if (!this.doctor.first_name || !this.doctor.last_name || !this.doctor.specialities) {
          throw {error: {message: 'Uzupełnij wszystkie pola'}};
      }
      const tokens = this.doctor.specialities.split(',').filter((token: string) => token.length !== 0);
      this.doctor.specialities = tokens;
      await this.doctorService.create(this.doctor);
      this.router.navigateByUrl('/admin/doctor-list');
    } catch (error) {
      console.log(error);
      if (error.error.message) {
        this.snackBar.open(error.error.message, 'Zamknij' , {duration: 2000});
      } else {
        this.snackBar.open(error, 'Zamknij' , {duration: 2000});
      }
    }
  }

}
