import { Component, OnInit } from '@angular/core';
import { DoctorService, IDoctor } from '../../../../services/doctor.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-doctor-details',
  templateUrl: './doctor-details.component.html',
  styleUrls: ['./doctor-details.component.css']
})
export class DoctorDetailsComponent implements OnInit {
  doctor: IDoctor;

  constructor(private doctorService: DoctorService, private route: ActivatedRoute) { }

  async ngOnInit() {
    await this.route.params.subscribe(async params => {
      this.doctor = await this.doctorService.getById(params.id);
    });
  }

}
