import { Component, OnInit, ViewChild } from '@angular/core';
import { DoctorService, IDoctor } from '../../../../services/doctor.service';
import { Router } from '@angular/router';
import { AbstractListComponent } from '../../../abstract/abstract-list.component';
import { MatSort } from '@angular/material';
@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})
export class DoctorListComponent extends AbstractListComponent<IDoctor> {

  constructor(protected doctorService: DoctorService, protected router: Router) {
    super(doctorService, router, 'doctor',
    ['first_name', 'last_name', 'details', 'edit', 'delete']);
  }


}
