import { Component, OnInit } from '@angular/core';
import { UserService, IUser } from '../../../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  user: IUser = {
    email: ''
  };
  title: string;

  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    await this.route.params.subscribe(async params => {
      this.user = await this.userService.getById(params.id);
      this.title = `Edytujesz użytkownika o id: ${this.user._id}`;
    });
  }
  async onSubmit() {
    await this.userService.update(this.user);
    this.router.navigateByUrl('/admin/user-list');
  }
}
