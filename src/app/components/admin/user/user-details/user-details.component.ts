import { Component, OnInit } from '@angular/core';
import { UserService, IUser } from '../../../../services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  user: IUser = {};
  constructor(private userService: UserService, private route: ActivatedRoute) { }

  async ngOnInit() {
    await this.route.params.subscribe(async params => {
      this.user = await this.userService.getById(params.id);
    });
  }

}
