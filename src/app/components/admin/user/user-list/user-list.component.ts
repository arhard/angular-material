import { Component, OnInit } from '@angular/core';
import { AbstractListComponent } from '../../../abstract/abstract-list.component';
import { IPatient } from '../../../../services/patient.service';
import { UserService } from '../../../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent extends AbstractListComponent<IPatient> {
  constructor(protected userService: UserService, protected router: Router) {
    super(userService, router, 'user',
    ['email', 'details', 'edit', 'delete']);
  }
}
