import { Component, OnInit, Input } from '@angular/core';
import { IUser } from '../../../../services/user.service';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.css']
})
export class RoleEditComponent implements OnInit {
  @Input()
  user: IUser;
  @Input()
  roleInput = '';

  constructor() { }

  ngOnInit() {
  }

  addRole(role) {
    this.user.roles.push(role);
    this.roleInput = '';
  }

  deleteRole(role, roleField) {
    console.log('delete:', role, roleField);
    this.user.roles = this.user.roles.filter(r => r !== role);
  }

}
