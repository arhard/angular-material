import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../../../node_modules/@angular/router';
import { ScheduleService, ISchedule } from '../../../../services/schedule.service';
import { PatientService } from '../../../../services/patient.service';

@Component({
  selector: 'app-schedule-details',
  templateUrl: './schedule-details.component.html',
  styleUrls: ['./schedule-details.component.css']
})
export class ScheduleDetailsComponent implements OnInit {

  schedule: ISchedule;

  constructor(private route: ActivatedRoute, private scheduleService: ScheduleService, private patientService: PatientService) { }

  async ngOnInit() {
    await this.route.params.subscribe(async params => {
      this.schedule = await this.scheduleService.getById(params.id);
      this.schedule.visits.forEach(async visit => visit.patient_id = await this.patientService.getById(visit.patient_id));
    });
  }

}
