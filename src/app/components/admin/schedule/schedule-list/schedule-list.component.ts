import { Component, OnInit } from '@angular/core';
import { AbstractListComponent } from '../../../abstract/abstract-list.component';

import { ISchedule, ScheduleService } from '../../../../services/schedule.service';
import { Router } from '@angular/router';
import { DoctorService } from '../../../../services/doctor.service';

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.css']
})
export class ScheduleListComponent extends AbstractListComponent<ISchedule> {
  constructor(protected scheduleService: ScheduleService, protected doctorService: DoctorService, protected router: Router) {
    super(scheduleService, router, 'schedule',
  ['start', 'end', 'doctor', 'clinic', 'details', 'edit', 'delete']);
  }

  async delete(id) {
    const schedule = await this.dataService.getById(id);
    this.dataSource.data = this.dataSource.data.filter(element => element._id !== id);
    const deleted = await this.dataService.delete(id);
    await this.doctorService.removeSchedule(schedule.doctor_id._id, id);
  }

}
