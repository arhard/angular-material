import { Component, OnInit } from '@angular/core';
import { ScheduleService, ISchedule } from '../../../../services/schedule.service';
import { ClinicService, IClinic } from '../../../../services/clinic.service';
import { DoctorService, IDoctor } from '../../../../services/doctor.service';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-schedule-edit',
  templateUrl: './schedule-edit.component.html',
  styleUrls: ['./schedule-edit.component.css']
})
export class ScheduleEditComponent implements OnInit {

  clinics: IClinic[];
  doctors: IDoctor[];
  clinic_id: any;
  doctor_id: any;

  date: Date;
  start: string;
  end: string;

  schedule: ISchedule;
  model: ISchedule;
  constructor(
    private scheduleService: ScheduleService,
    private clinicService: ClinicService,
    private doctorService: DoctorService,
    private router: Router,
    private route: ActivatedRoute) {

    }

  async ngOnInit() {
    this.clinics = <IClinic[]>await this.clinicService.get();
    this.doctors = <IDoctor[]>await this.doctorService.get();
    await this.route.params.subscribe(async params => {
      this.model = await this.scheduleService.getById(params.id);
    });


  }

  async onSubmit() {
    try {
      // if new date is selected
      if (this.date) {
        this.model = this.scheduleService.changeDateWithoutTime(this.date, this.model);
      }

      // if new star hour
      this.model.start = this.start ? this.scheduleService.getDateTime(this.model.start, this.start) : this.model.start;
      // if new end hour
      this.model.end = this.end ? this.scheduleService.getDateTime(this.model.end, this.end) : this.model.end;
      // if new clinic
      this.model.clinic_id = this.clinic_id || this.model.clinic_id;
      // if new doctor
      this.model.doctor_id = this.doctor_id || this.model.doctor_id;

      // update
      await this.scheduleService.update(this.model);
      // and redirect
      this.router.navigateByUrl('/admin/schedule-list');
    } catch (error) {
      console.log(this.model);
      alert('Wystąpił błąd: ' + (error.error || error.message));
      console.log(error);
    }
  }

}
