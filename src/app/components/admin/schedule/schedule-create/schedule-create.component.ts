import { Component, OnInit } from '@angular/core';
import { ScheduleService, ISchedule } from '../../../../services/schedule.service';
import { ClinicService, IClinic } from '../../../../services/clinic.service';
import { DoctorService, IDoctor } from '../../../../services/doctor.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { MatSnackBar } from '../../../../../../node_modules/@angular/material';

@Component({
  selector: 'app-schedule-create',
  templateUrl: './schedule-create.component.html',
  styleUrls: ['./schedule-create.component.css']
})
export class ScheduleCreateComponent implements OnInit {
  clinics: IClinic[];
  doctors: IDoctor[];
  clinic_id: any;
  doctor_id: any;

  date: Date;
  start: string;
  end: string;

  schedule: ISchedule;

  constructor(
    private scheduleService: ScheduleService,
    private clinicService: ClinicService,
    private doctorService: DoctorService,
    private router: Router,
    public snackBar: MatSnackBar) {

    }

  async ngOnInit() {
    this.clinics = <IClinic[]>await this.clinicService.get();
    this.doctors = <IDoctor[]>await this.doctorService.get();
  }

  async onSubmit() {
    try {
      // check if all fields are filed
      if (!this.date || !this.start || !this.end || !this.clinic_id || !this.doctor_id) {
        throw new Error('Wszystkie pola muszą być uzupełnione.');
      }

      const start_date = moment(this.scheduleService.getDateTime(this.date, this.start));
      const end_date = moment(this.scheduleService.getDateTime(this.date, this.end));

      if (end_date.isBefore(start_date) || end_date.isSame(start_date)) {
        throw new Error('Data początkowa musi być przed końcową');
      }

      // prepare schedule data
      this.schedule = {
        clinic_id: this.clinic_id,
        doctor_id: this.doctor_id,
        start: start_date.toDate(),
        end: end_date.toDate(),
        visits: []
      };

      // send to server
      const savedSchedule = await this.scheduleService.create(this.schedule);

      // add shecule to doctor
      await this.doctorService.insertSchedule(this.doctor_id, (<ISchedule>savedSchedule)._id);

      // redirect to schedule list
      this.router.navigateByUrl('/admin/schedule-list');
    } catch (error) {
      // notify about errors
      this.snackBar.open(error.error || error.message, 'Zamknij', {duration: 2000});
      console.log(error);
    }
  }
}
