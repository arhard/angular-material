import { Component, OnInit } from '@angular/core';
import { IPatient, PatientService } from '../../../../services/patient.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.css']
})
export class PatientEditComponent implements OnInit {
  title = 'Edycja pacjenta';
  model: IPatient = {};
  fields = [
    {name: 'first_name', placeholder: 'Imie'},
    {name: 'last_name', placeholder: 'Nazwisko'},
    {name: 'national_number', placeholder: 'PESEL'}
  ];
  constructor(private patientService: PatientService, private router: Router, private route: ActivatedRoute) { }
  async ngOnInit() {
    await this.route.params.subscribe(async params => {
      this.model = await this.patientService.getById(params.id);
    });
  }
  async submited(value: IPatient) {
    value._id = this.model._id;
    await this.patientService.update(value);
    this.router.navigateByUrl('/admin/patient-list');
  }

}
