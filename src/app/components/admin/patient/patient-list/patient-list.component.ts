import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IPatient, PatientService } from '../../../../services/patient.service';
import { AbstractListComponent } from '../../../abstract/abstract-list.component';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent extends AbstractListComponent<IPatient> {
  constructor(protected patientService: PatientService, protected router: Router) {
    super(patientService, router, 'patient',
    ['first_name', 'last_name', 'national_number', 'assigned_user', 'details', 'edit', 'delete']);
  }
  async delete(id) {
    this.dataSource.data = this.dataSource.data.filter(element => element._id !== id);
    await this.patientService.deleteVisits(id);
    await this.dataService.delete(id);

  }
}
