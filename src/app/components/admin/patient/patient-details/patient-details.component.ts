import { Component, OnInit } from '@angular/core';
import { IPatient, PatientService } from '../../../../services/patient.service';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.css']
})
export class PatientDetailsComponent implements OnInit {
  patient: IPatient = {};
  constructor(private patietnService: PatientService, private route: ActivatedRoute) { }

  async ngOnInit() {
    await this.route.params.subscribe(async params => {
      this.patient = await this.patietnService.getById(params.id);
    });
  }

}
