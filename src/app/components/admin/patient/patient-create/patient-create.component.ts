import { Component, OnInit } from '@angular/core';
import { IPatient, PatientService } from '../../../../services/patient.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-patient-create',
  templateUrl: './patient-create.component.html',
  styleUrls: ['./patient-create.component.css']
})
export class PatientCreateComponent implements OnInit {
  title = 'Tworzeni pacjenta';
  model: IPatient = {};
  fields = [
    {name: 'first_name', placeholder: 'Imie'},
    {name: 'last_name', placeholder: 'Nazwisko'},
    {name: 'national_number', placeholder: 'PESEL'}
  ];
  constructor(private patientService: PatientService, private router: Router) { }
  ngOnInit() {
  }
  async submited(value: IPatient) {
    if (value.first_name && value.last_name && value.national_number) {
      await this.patientService.create(value);
      this.router.navigateByUrl('/admin/patient-list');
    } else {
      alert('Uzupełnij wszystkie pola');
    }
  }
}
