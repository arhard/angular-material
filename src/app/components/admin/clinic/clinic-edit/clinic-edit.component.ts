import { Component, OnInit } from '@angular/core';
import { IClinic, ClinicService } from '../../../../services/clinic.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '../../../../../../node_modules/@angular/material';

@Component({
  selector: 'app-clinic-edit',
  templateUrl: './clinic-edit.component.html',
  styleUrls: ['./clinic-edit.component.css']
})
export class ClinicEditComponent implements OnInit {
  title = 'Edycja kliniki';
  model: IClinic = {};
  fields = [
    {name: 'name', placeholder: 'Nazwa'},
    {name: 'country', placeholder: 'Kraj'},
    {name: 'postal_code', placeholder: 'Kod pocztowy'},
    {name: 'city', placeholder: 'Miasto'},
    {name: 'street', placeholder: 'Ulica'},
    {name: 'building_number', placeholder: 'Numer budynku'},
  ];
  constructor(public snackBar: MatSnackBar, private clinicService: ClinicService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    await this.route.params.subscribe(async params => {
      console.log('params:', params.id);
      this.model._id = params.id;
      console.log('model', this.model);
    });
  }

  async submited(value: IClinic) {

    try {
    value._id = this.model._id;
    await this.clinicService.update(value);
    this.router.navigateByUrl('/admin/clinic-list');
    } catch (error) {
      let message = '';
      for (const property in error.error.errors) {
        if (error.error.errors.hasOwnProperty(property)) {
          message += error.error.errors[property].message + ' ';
        }
      }
      this.snackBar.open(message, 'Zamknij', {duration: 5000});
    }
  }

}
