import { Component, OnInit } from '@angular/core';
import { AbstractCreateComponents } from '../../../abstract/abstract-create.component';
import { IClinic, ClinicService } from '../../../../services/clinic.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '../../../../../../node_modules/@angular/material';

@Component({
  selector: 'app-clinic-create',
  templateUrl: './clinic-create.component.html',
  styleUrls: ['./clinic-create.component.css']
})
export class ClinicCreateComponent extends AbstractCreateComponents<IClinic> {

  constructor(public snackBar: MatSnackBar, protected clinicService: ClinicService, protected router: Router) {
    super(snackBar, clinicService, router, 'clinic', ['name', 'country', 'postal_code', 'city', 'street', 'building_number']);
  }
}
