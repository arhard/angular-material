import { Component, OnInit } from '@angular/core';
import { ClinicService, IClinic } from '../../../../services/clinic.service';
import { AbstractListComponent } from '../../../abstract/abstract-list.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clinic-list',
  templateUrl: './clinic-list.component.html',
  styleUrls: ['./clinic-list.component.css']
})
export class ClinicListComponent extends AbstractListComponent<IClinic> {
  constructor(protected clinicService: ClinicService, protected router: Router) {
    super(clinicService, router, 'clinic',
    ['name', 'country', 'postal_code', 'city', 'street', 'building_number', 'edit', 'delete']);
   }
}
