import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../../../node_modules/@angular/router';
import { UserService } from '../../../../services/user.service';
import { VisitService, IVisit } from '../../../../services/visit.service';
import { DoctorService } from '../../../../services/doctor.service';

@Component({
  selector: 'app-visit-details',
  templateUrl: './visit-details.component.html',
  styleUrls: ['./visit-details.component.css']
})
export class VisitDetailsComponent implements OnInit {

  visit: IVisit;

  constructor(protected route: ActivatedRoute, protected visitService: VisitService, protected doctorService: DoctorService) { }

  async ngOnInit() {
    await this.route.params.subscribe(async params => {
      this.visit = await this.visitService.getById(params.id);
      this.visit.schedule_id.doctor_id = await this.doctorService.getById(this.visit.schedule_id.doctor_id);
    });
  }

}
