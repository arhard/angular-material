import { Component, OnInit } from '@angular/core';
import { ISchedule, ScheduleService } from '../../../../services/schedule.service';

@Component({
  selector: 'app-visit-create',
  templateUrl: './visit-create.component.html',
  styleUrls: ['./visit-create.component.css']
})
export class VisitCreateComponent implements OnInit {
  start: string;
  end: string;
  date: Date;

  schedules: ISchedule[];

  constructor(private scheduleService: ScheduleService) { }

  async ngOnInit() {
  }

  onSubmit() {

  }

  async dateInput(event) {
    console.log(event.value);
    // pobranie grafików
    this.schedules = <ISchedule[]>(await this.scheduleService.get());

    // this.schedules.forEach(schedule => console.log(new Date(schedule.start).getDate()));
    // wybranie grafików na ten dzień
    this.schedules = this.schedules.filter(schedule => {
      const date = new Date(schedule.start);
      return date.getDay() === this.date.getDay()
      && date.getDate() === this.date.getDate()
      && date.getFullYear() === this.date.getFullYear();
    });

    console.log(this.schedules);
  }

}
