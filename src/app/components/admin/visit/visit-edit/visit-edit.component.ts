import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { FormGroup, FormBuilder } from '@angular/forms';
import { EditVisitData } from '../visit-list/visit-list.component';


@Component({
  selector: 'app-visit-edit',
  templateUrl: './visit-edit.component.html',
  styleUrls: ['./visit-edit.component.css']
})
export class VisitEditComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<VisitEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditVisitData
  ) {

  }

  ngOnInit() {
  }

}
