import { Component, OnInit } from '@angular/core';
import { AbstractListComponent } from '../../../abstract/abstract-list.component';
import { IVisit, VisitService } from '../../../../services/visit.service';
import { Router } from '../../../../../../node_modules/@angular/router';
import { MatDialog } from '../../../../../../node_modules/@angular/material';
import { VisitEditComponent } from '../visit-edit/visit-edit.component';

export interface EditVisitData {
  canceled: boolean;
  completed: boolean;
}

@Component({
  selector: 'app-visit-list',
  templateUrl: './visit-list.component.html',
  styleUrls: ['./visit-list.component.css']
})
export class VisitListComponent extends AbstractListComponent<IVisit> {

  editedData: EditVisitData;

  constructor(public dialog: MatDialog, protected visitService: VisitService, protected router: Router) {
    super(visitService, router, 'visit', ['start', 'end', 'reason', 'completed', 'canceled', 'details', 'edit', 'delete']);
  }

  /**
   * Open edit dialog and updates visit
   * @param id id of visit that is being edited
   */
  async editDialog(id: string) {
    // angular material requires o pass some data to the dialog so this has to be set
    this.editedData = {
      canceled: false,
      completed: false
    };

    // open dialog and pass data
    const dialogRef = this.dialog.open(VisitEditComponent, {
      width: '200px', data: this.editedData
    });
    // process output
    dialogRef.afterClosed().toPromise().then(async result => {
      // update visit
      await this.visitService.update({
        _id: id,
        canceled: result.canceled,
        completed: result.completed
      });

      // update visit list
      const visit: IVisit = this.dataSource.data.find(v => v._id === id);
      visit.canceled = result.canceled;
      visit.completed = result.completed;
    });
  }

}
