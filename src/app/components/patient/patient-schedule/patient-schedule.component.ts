import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ISchedule } from '../../../services/schedule.service';


@Component({
  selector: 'app-patient-schedule',
  templateUrl: './patient-schedule.component.html',
  styleUrls: ['./patient-schedule.component.css']
})
export class PatientScheduleComponent implements OnInit {
  @Input()
  schedule: ISchedule;

  @Output()
  scheduleSelected = new EventEmitter<ISchedule>();

  constructor() {}

  ngOnInit() {
  }

  selected() {
    this.scheduleSelected.emit(this.schedule);
  }

}
