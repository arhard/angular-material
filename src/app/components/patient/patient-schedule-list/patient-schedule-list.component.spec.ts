import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientScheduleListComponent } from './patient-schedule-list.component';

describe('PatientScheduleListComponent', () => {
  let component: PatientScheduleListComponent;
  let fixture: ComponentFixture<PatientScheduleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientScheduleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientScheduleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
