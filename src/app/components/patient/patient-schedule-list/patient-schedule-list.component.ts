import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { ISchedule } from '../../../services/schedule.service';
import { MatSelectChange, MatStepper } from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import * as moment from 'moment';

@Component({
  selector: 'app-patient-schedule-list',
  templateUrl: './patient-schedule-list.component.html',
  styleUrls: ['./patient-schedule-list.component.css']
})
export class PatientScheduleListComponent implements OnInit, OnChanges {
  // schedules optained from parrent component, these schedules wont be
  // modifed during filtration
  @Input()
  schedules: ISchedule[];

  // outputs selected schedule from the list
  @Output()
  selectedSchedule = new EventEmitter<ISchedule>();

  // filtrated schedules, these will be modified accroding to user's query
  filteredSchedules: ISchedule[];

  // filter option selected by the user
  currentFilter: string;

  // value typed in to the filter field, schedule list will be filtered according to this value
  filterValue: string;

  // filter field is disabled unless the user selects filtration type
  filterFieldDisabled = true;


  constructor() {}

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.schedules) {
      this.filteredSchedules = this.schedules.slice(0);

      // sort by time ascending
      this.filteredSchedules = this.filteredSchedules.sort((a: ISchedule, b: ISchedule) => {
         return (new Date(a.start).getTime() - new Date(b.start).getTime());
      });
    }
  }

  resetStepper(stepper: MatStepper) {
    stepper.selectedIndex = 0;
  }

  // sets filter value after it's changed
  filterSelected(event: MatSelectChange) {
    this.filterFieldDisabled = false;
    this.currentFilter = event.value;
  }

  /**
   * Filters schedules based on a selected filter type and value typed by user
   * @param value filter value
   */
  onFilterChange(value: string): void {
    if (value.length === 0) {
      this.filteredSchedules = this.schedules.slice(0);
      return;
    }

    switch (this.currentFilter) {
      case 'city':
        this.filteredSchedules = this.schedules
          .filter((schedule: ISchedule) => schedule.clinic_id.city.toLowerCase().startsWith(value.toLowerCase()));
      break;
      case 'doctor':
      this.filteredSchedules = this.schedules
        .filter((schedule: ISchedule) => `${schedule.doctor_id.first_name} ${schedule.doctor_id.last_name}`.toLowerCase().startsWith(value.toLowerCase()));
      break;
      case 'clinic':
      this.filteredSchedules = this.schedules
        .filter((schedule: ISchedule) => schedule.clinic_id.name.toLowerCase().startsWith(value.toLowerCase()));
      break;

    }
  }

  scheduleSelected(event: ISchedule) {
    this.selectedSchedule.emit(event);
  }
}
