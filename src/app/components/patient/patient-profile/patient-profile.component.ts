import { Component, OnInit } from '@angular/core';
import { PatientService, IPatient } from '../../../services/patient.service';
import { AuthService, UserCredentials } from '../../../services/auth.service';
import { UserService, IUser } from '../../../services/user.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-patient-profile',
  templateUrl: './patient-profile.component.html',
  styleUrls: ['./patient-profile.component.css']
})
export class PatientProfileComponent implements OnInit {
  credentials: UserCredentials = undefined;
  user: IUser;
  patient: IPatient;

  hasUser = false;

  // first_name: string;
  // last_name: string;
  // national_number: string;
  constructor(public snackBar: MatSnackBar, private patientService: PatientService, private userService: UserService, private auth: AuthService) {
    this.patient = {
      first_name: '',
      last_name: '',
      national_number: '',
      user_id: {},
    };
  }

  async ngOnInit() {
    try {
      // get user credentials
      this.credentials = this.auth.getCredentials();
      // check if user has patient  profile
      this.hasUser = await this.patientService.hasUser();

      // if user has a patient profile then request it
      if (this.hasUser) {
        this.patient = await this.patientService.getByCurrentUsersCredentials();
      }
    } catch (error) {
      console.log('error', error);
    }
  }

  async onSubmit() {
    // if not all fields are filled then exit the function
    if (!this.patient.first_name || !this.patient.last_name || !this.patient.national_number) {
      return;
    }

    // set user_id
    this.patient.user_id = this.credentials.id;


    try {
      // and save it
      await this.patientService.create(this.patient);

      // now patient has connected user to it
      this.hasUser = true;
    } catch (error) {
      console.log(error);
      this.snackBar.open('Nie udało się usawić profilu', 'Zamknij', {duration: 2000});
    }

  }

}
