import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientScheduleVisitCreationComponent } from './patient-schedule-visit-creation.component';

describe('PatientScheduleVisitCreationComponent', () => {
  let component: PatientScheduleVisitCreationComponent;
  let fixture: ComponentFixture<PatientScheduleVisitCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientScheduleVisitCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientScheduleVisitCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
