import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientVisitCancelComponent } from './patient-visit-cancel.component';

describe('PatientVisitCancelComponent', () => {
  let component: PatientVisitCancelComponent;
  let fixture: ComponentFixture<PatientVisitCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientVisitCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientVisitCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
