import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-patient-visit-cancel',
  templateUrl: './patient-visit-cancel.component.html',
  styleUrls: ['./patient-visit-cancel.component.css']
})
export class PatientVisitCancelComponent implements OnInit {

  confirmButtonDisabled = !this.data;
  constructor(public dialogRef: MatDialogRef<PatientVisitCancelComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
