import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientVisitListComponent } from './patient-visit-list.component';

describe('PatientVisitListComponent', () => {
  let component: PatientVisitListComponent;
  let fixture: ComponentFixture<PatientVisitListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientVisitListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientVisitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
