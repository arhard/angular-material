import { Component, OnInit } from '@angular/core';
import { PatientService, IPatient } from '../../../services/patient.service';
import { IVisit, VisitService } from '../../../services/visit.service';
import { ScheduleService, ISchedule } from '../../../services/schedule.service';
import { MatDialog } from '../../../../../node_modules/@angular/material';
import { PatientVisitCancelComponent } from '../patient-visit-cancel/patient-visit-cancel.component';
import { Router } from '../../../../../node_modules/@angular/router';


@Component({
  selector: 'app-patient-visit-list',
  templateUrl: './patient-visit-list.component.html',
  styleUrls: ['./patient-visit-list.component.css']
})
export class PatientVisitListComponent implements OnInit {
  patientId: string;
  patient: IPatient;
  visits: IVisit[];
  cancelationReason = '';

  displayedColumns = ['date', 'start', 'end', 'reason', 'doctor', 'clinic', 'address', 'cancel'];

  constructor(private router: Router, public dialog: MatDialog, private patientService: PatientService, private scheduleService: ScheduleService, private visitService: VisitService) { }

  async ngOnInit() {
    this.patient = await this.patientService.getByCurrentUsersCredentials();
    this.visits = this.patient.visits.filter(visit => !visit.canceled);
    this.patient.visits = this.visits;
    this.patient.visits.forEach(async visit => visit.schedule_id = await this.scheduleService.getById(<string>visit.schedule_id));
  }

  openDialog(id): void {
    const dialogRef = this.dialog.open(PatientVisitCancelComponent, {
      width: '250px',
      data: {cancelationReason: this.cancelationReason}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.cancelVisit(id, result);
      }
    });
  }

  async cancelVisit(id: string, reason: string) {
    await this.visitService.cancelVisit(id, reason);

    // mark visit as canceled
    this.visits
      .filter(visit => visit._id === id)
      .forEach(visit => visit.canceled = true);

    // filer out canceled
    this.visits = this.patient.visits.filter(visit => !visit.canceled);
  }
}
