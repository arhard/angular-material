import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientCreateVisitComponent } from './patient-create-visit.component';

describe('PatientCreateVisitComponent', () => {
  let component: PatientCreateVisitComponent;
  let fixture: ComponentFixture<PatientCreateVisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientCreateVisitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientCreateVisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
