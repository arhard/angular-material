import { Component, OnInit } from '@angular/core';
import { MatDatepickerInputEvent, MatSnackBar } from '@angular/material';
import { ISchedule, ScheduleService } from '../../../services/schedule.service';
import { MatSelectChange, MatStepper } from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { IVisit, VisitService } from '../../../services/visit.service';
import * as moment from 'moment';
import { PatientService, IPatient } from '../../../services/patient.service';
import { AuthService } from '../../../services/auth.service';
import { IUser } from '../../../services/user.service';
import { Router } from '@angular/router';
import { VisitCreateComponent } from '../../admin/visit/visit-create/visit-create.component';

@Component({
  selector: 'app-patient-create-visit',
  templateUrl: './patient-create-visit.component.html',
  styleUrls: ['./patient-create-visit.component.css']
})
export class PatientCreateVisitComponent implements OnInit {
  // patient cant sign for a visit in the past, this date is used to limit dates to at least today
  minDate = new Date();
  start: Date;
  end: Date;
  schedules: ISchedule[];
  selectedHour: string;


  schedule: ISchedule;
  availableHours: string[];

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  constructor(private snackBar: MatSnackBar, private router: Router, private visitService: VisitService, private auth: AuthService, private patientService: PatientService, private scheduleService: ScheduleService, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      start_date: ['', Validators.required],
      end_date: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({

    });
    this.thirdFormGroup = this._formBuilder.group({
      hour: ['', Validators.required],
      visitReason: ['', Validators.required]
    });
  }

  resetStepper(stepper: MatStepper ) {
    stepper.selectedIndex = 0;
  }

  startDateInput(event) {
    this.start = event.value;
  }
  endDateInput(event) {
    this.end = event.value;
  }

  async showSchedules() {
    if (!this.start || !this.end) {
      return;
    }

    this.schedules = await this.scheduleService.findBetweenDates(this.start, this.end);
  }

  selectedSchedule(event: ISchedule, stepper: MatStepper) {
    this.schedule = event;
    stepper.next();

    // calculate available hours
    this.availableHours = this.scheduleService.calculateAvailableHours(this.schedule);
  }

  async saveVisit(visitReason, selectedHour) {
    try {
      // get current patient
      const patient = <IPatient>await this.patientService.getByCurrentUsersCredentials();

      // prepare visit date
      // set hour accoirding to user's choice
      const visit_start = this.scheduleService.getDateTime(this.schedule.start, selectedHour);
      // set end date with 30 minutes offset
      const visit_end = moment(visit_start).add(30, 'minutes').toDate();
      if (!visit_start || !visitReason) {
        throw new Error('Wszystkie pola muszą być uzupełnione');
      }
      // prepare data for sending
      const visit: IVisit = {
        start: visit_start,
        end: visit_end,
        reason: visitReason,
        completed: false,
        canceled: false,
        schedule_id: this.schedule._id,
        patient_id: patient._id
      };


      // console.log(`this visit's start:`, visit.start);
      // check if patient isn't already signed for a visit for that date

      const visitsCountAtTheSameDate = patient.visits
        .filter(v => moment(new Date(v.start)).isSame(moment(visit.start))).length;
      if (visitsCountAtTheSameDate !== 0) {
        throw new Error('Jestes już zapisany na wizytę w tym terminie');
      }

      console.log('visit to be created', visit);
      // save visit
      const createdVisit = await this.visitService.create(visit);
      // add reference to patient
      console.log(await this.patientService.insertVisit(createdVisit));
      // add reference to schedule
      console.log(await this.scheduleService.insertVisit(createdVisit));
      // redirect to visit list
      this.router.navigateByUrl('patient/visit-list');
    } catch (error) {
      this.snackBar.open(error.message, 'Zamknij');
    }
  }


}
