import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractCreateComponent } from './abstract-create.component';

describe('AbstractCreateComponent', () => {
  let component: AbstractCreateComponent;
  let fixture: ComponentFixture<AbstractCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbstractCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
