import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-abstract-create',
  templateUrl: './abstract-create.component.html',
  styleUrls: ['./abstract-create.component.css']
})
export class AbstractCreateComponent implements OnInit {
  @Input()
  title: string;

  @Input()
  model: any;

  @Input()
  fields: any[];

  @Output()
  submited: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    this.submited.emit(this.model);
  }

}
