import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { Router } from '../../../../node_modules/@angular/router';
import { MatSnackBar } from '../../../../node_modules/@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  password: string;

  constructor(public snackBar: MatSnackBar, private http: HttpClient, private auth: AuthService, private router: Router) { }

  ngOnInit() {
    // this.auth.redirectOnLogin();
  }

  async onSubmit() {
    if (!this.email || !this.password) {
      this.snackBar.open('Uzupełnij wszystkie pola', 'Zamknij', {duration: 2000});
      return;
    }
    try {
      await this.auth.register(this.email, this.password);
    } catch (error) {
      this.snackBar.open(error.error || error.message, 'Zamknij', {duration: 2000});
    }
  }
}
